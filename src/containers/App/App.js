import React, { Component } from 'react';
import './App.css';
import Menu from "../../components/Menu/Menu";
import Order from "../../components/Order/Order";
import burger from '../../assets/burger.jpg';
import cheeasburger from '../../assets/cheeseburger.png';
import coffee from '../../assets/coffee.png';
import cola from '../../assets/cola.png';
import fries from '../../assets/fries.png'
import tea from '../../assets/tea.png';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order: {}
    };

    this.menu = {
      hamburger: {
        price: 80,
        img: burger
      },
      cheeseburger: {
        price: 90,
        img: cheeasburger
      },
      fries: {
        price: 45,
        img: fries
      },
      coffee: {
        price: 70,
        img: coffee
      },
      tea: {
        price: 50,
        img: tea
      },
      cola: {
        price: 40,
        img: cola
      }
    };
  }

  addOrder = key => {
    let item = this.state.order[key];
    if (!item){
      item = 1;
    } else {
      item++;
    }

    const order = {...this.state.order};
    order[key] = item;

    this.setState({order});
  };

  removeOrder = key => {
    const order = {...this.state.order};
    delete order[key];

    this.setState({order})
  };

  render() {
    return (
      <div className="App">
        <Order order={this.state.order} menu={this.menu} click={this.removeOrder.bind(this)}/>
        <Menu menu={this.menu} click={this.addOrder.bind(this)}/>
        <img src="" alt=""/>
      </div>
    );
  }
}

export default App;
