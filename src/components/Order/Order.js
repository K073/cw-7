import React from 'react';
import OrderItem from "./OrderItem/OrderItem";
import "./Order.css"

const Order = props => {
  const order = [];

  let fullPrice = 0;

  for (let key in props.order){
    order.push(<OrderItem key={key} name={key} count={props.order[key]} price={props.menu[key].price} click={() => props.click(key)}/>)
    fullPrice+= props.menu[key].price * props.order[key];
  }

  const totalPrice = order.length > 0 ? <div>Total price :{fullPrice}</div> : null;
  const orderList = order.length > 0 ? order  : <span>Order is empty!<br/>Please add some items!</span>;

  return (
    <div className='Order'>
      {orderList}
      {totalPrice}
    </div>
  );
};

export default Order;