import React from 'react';
import './OrderItem.css'

const OrderItem = props => {
  return (
    <div className="OrderItem">
      <span className="OrderItem-name">{props.name}</span>
      <span>{props.count}</span>
      <span>{props.menu} KGS</span>
      <button className="OrderItem-btn" onClick={props.click}>X</button>
    </div>
  );
};

export default OrderItem;