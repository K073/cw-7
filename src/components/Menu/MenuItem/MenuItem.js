import React from 'react';
import './MenuItem.css';

const MenuItem = props => {
  return (
    <div className="MenuItem" onClick={props.click}>
      <img className="MenuItem-icon" src={props.img} alt={props.name}/>
      <div className="MenuItem-descr">
        <div className="MenuItem-name">{props.name}</div>
        <div className="MenuItem-price">Price: {props.price} KGS</div>
      </div>
    </div>
  );
};


export default MenuItem;