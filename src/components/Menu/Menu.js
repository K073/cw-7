import React from 'react';
import MenuItem from "./MenuItem/MenuItem";
import './Menu.css';

const Menu = props => {
  const menu = [];

  for (let key in props.menu){
    menu.push(<MenuItem key={key} name={key} img={props.menu[key].img} price={props.menu[key].price} click={() => props.click(key)}/>)
  }

  return (
    <div className="Menu">
      {menu}
    </div>
  );
};

export default Menu;